//
//  Song+CoreDataProperties.swift
//  Sempre
//
//  Created by Yash Rajana on 3/3/19.
//  Copyright © 2019 Yash Rajana. All rights reserved.
//
//

import UIKit
import CoreData


extension Song {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Song> {
        return NSFetchRequest<Song>(entityName: "Song")
    }

    @NSManaged public var beatsPerMeasure: Int16
    @NSManaged public var composer: String?
    @NSManaged public var beatsPerMinute: Int16
    @NSManaged public var title: String?
    @NSManaged public var pageImages: [UIImage]
    @NSManaged public var pageMeasureNumbers: [Int]

}
